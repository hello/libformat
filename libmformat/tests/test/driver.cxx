// file: tests/test/driver.cxx -*- C++ -*-

import std.core;
import std.io;
import format;

int
main ()
{
  using namespace std;
  using namespace format;

  cout << message ("Hello", "World", volume::quiet) << endl;
  cout << message ("Hello", "World", volume::normal) << endl;
  cout << message ("Hello", "World", volume::loud) << endl;
}
