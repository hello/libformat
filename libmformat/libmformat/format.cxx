// file: libmformat/format.cxx -*- C++ -*-

module format;

import std.core; // transform()
import std.io;   // to{upper,lower}()

using namespace std;

namespace format
{
  string
  message (const string& g, const string& n, volume v)
  {
    string r (g);

    transform (r.begin (), r.end (), r.begin (),
               [v] (char c) -> char
               {
                 switch (v)
                 {
                 case volume::quiet:  return tolower (c);
                 case volume::normal: break;
                 case volume::loud:   return toupper (c);
                 }
                 return c;
               });

    return r += ", " + n + '!';
  }
}
