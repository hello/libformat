#include <libformat/format.hxx>

#include <cctype>     // to{upper,lower}()
#include <algorithm>  // transform()
#include <stdexcept>

using namespace std;

namespace format
{
  string
  format_hello (const string& g, const string& n, volume v)
  {
    if (const char* w = (g.empty () ? "empty greeting" :
                         n.empty () ? "empty name"     : nullptr))
      throw invalid_argument (w);

    string h (g);

    transform (h.begin (), h.end (), h.begin (),
               [v] (char c) -> char
               {
                 switch (v)
                 {
                 case volume::quiet:  return tolower (c);
                 case volume::normal: break;
                 case volume::loud:   return toupper (c);
                 }
                 return c;
               });

    return h += ", " + n + '!';
  }
}
