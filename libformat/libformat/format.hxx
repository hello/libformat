#pragma once

#include <string>

#include <libformat/export.hxx>

namespace format
{
  enum class volume {quiet, normal, loud};

  LIBFORMAT_SYMEXPORT std::string
  format_hello (const std::string& greeting,
                const std::string& name,
                volume = volume::normal);
}
