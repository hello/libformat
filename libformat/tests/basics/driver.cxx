#include <stdexcept>

#include <libformat/version.hxx>
#include <libformat/format.hxx>

#undef NDEBUG
#include <cassert>

int main ()
{
  using namespace std;
  using namespace format;

  // Basics.
  //
  assert (format_hello ("Hello", "World", volume::quiet)  == "hello, World!");
  assert (format_hello ("Hello", "World", volume::normal) == "Hello, World!");
  assert (format_hello ("Hello", "World", volume::loud)   == "HELLO, World!");

  // Empty greeting.
  //
  try
  {
    format_hello ("", "World");
    assert (false);
  }
  catch (const invalid_argument& e)
  {
    assert (e.what () == string ("empty greeting"));
  }

  // Empty name.
  //
  try
  {
    format_hello ("Hello", "");
    assert (false);
  }
  catch (const invalid_argument& e)
  {
    assert (e.what () == string ("empty name"));
  }
}
